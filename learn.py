# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 09:52:35 2017

@author: Administrator
"""
#Importing all the needed packages
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import itertools
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.cross_validation import KFold


#Reading in the data
df = pd.read_csv('./diabetes.csv')
df.isnull().sum() #Checking for NULL values in the dataset
#Now performing Basic Exploratory Data Analysis

#PLotting Histplots of all the features in the dataset
columns=df.columns[:8]
plt.subplots(figsize=(18,15))
length=len(columns)
for i,j in itertools.zip_longest(columns,range(length)):
    plt.subplot((length/2),3,j+1)
    plt.subplots_adjust(wspace=0.2,hspace=0.5)
    df[i].hist(bins=20,edgecolor='black')
    plt.title(i)
plt.show()

#Splitting dataset into train and test datasets

X = df.drop('Outcome', axis=1)
Y = df.Outcome

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.3, stratify = df['Outcome'])

'''Testing Different Algorithms the long way'''
#Trying SVMs on the dataset
types = ['rbf','linear']
for i in types:
    model_svm = svm.SVC(kernel = i)
    model_svm.fit(X_train, Y_train)
    pred = model_svm.predict(X_test)
    print("SVM accuracy for kernal ", i ," is =", metrics.accuracy_score(pred, Y_test))

#Trying Logistic Regression
model_LR = LogisticRegression()
model_LR.fit(X_train, Y_train)
pred = model_LR.predict(X_test)
print("Accuracy for Logistic Regression is = ", metrics.accuracy_score(pred, Y_test))

#Trying Decision Trees
model_tree = DecisionTreeClassifier()
model_tree.fit(X_train, Y_train)
pred = model_tree.predict(X_test)
print("Accuracy for Decision Trees is = ", metrics.accuracy_score(pred, Y_test))

#Trying KNN classification
a_index=list(range(1,11))
a=pd.Series()
x=[0,1,2,3,4,5,6,7,8,9,10]
for i in list(range(1,11)):
    model_KNN=KNeighborsClassifier(n_neighbors=i) 
    model_KNN.fit(X_train,Y_train)
    pred=model_KNN.predict(X_test)
    a=a.append(pd.Series(metrics.accuracy_score(pred,Y_test)))
plt.plot(a_index, a)
plt.xticks(x)
plt.show()
print('Accuracies for different values of n are:',a.values)

'''Testing Different ML algorithms the short way.'''
abc = []
classifiers = ['Linear SVM', 'Radial SVM', 'Logistic Regression', 'KNN', 'Decision Trees']
models = [svm.SVC(kernel = 'linear'), svm.SVC(kernel = 'rbf'), LogisticRegression(), KNeighborsClassifier(), DecisionTreeClassifier()]

for i in models:
    model = i
    model.fit(X_train, Y_train)
    pred = model.predict(X_test)
    abc.append(metrics.accuracy_score(pred,Y_test))
models_df=pd.DataFrame(abc,index=classifiers)  
models_df.columns = ['Accuracy']
print(models_df)

'''To do feature selection and Ensembling 
Edit: Doing it now'''


