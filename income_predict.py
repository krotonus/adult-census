# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 04:07:43 2017

@author: HOME
"""
import pandas as pd
import seaborn as sns
import numpy as np
from sklearn import model_selection
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC


names = ['Age','Workclass','fnlgwt','Education','Education Num','Marital Status',
           'Occupation','Relationship','Race','Sex','Capital Gain','Capital Loss',
           'Hours/Week','Country','Above/Below 50K']
df = pd.read_csv("./adult-training.csv", encoding = 'utf-8', names = names, sep = ', ')
df_raw = df
#missing columns = Workclass, Occupation, COuntry
df = df[df['Workclass'] != '?']
df = df[df['Occupation'] != '?']
df = df[df['Country'] != '?']

#Removing missing values
'''missing = df.Workclass.iloc[32542]
df = df[df.Workclass != missing]
df = df[df.Occupation != missing]
df = df[df.Country != missing]
I don't know the reason why pandas was behaving this way. Made an ugly hack to remove the values tho.

Edit: found it, the value in the dataframe is ---> "'?'"  
Don't know how to change it to regular question mark'''

'''Edit2: Figured it. The separator was ', '''

#Mapping the string values in the columns to numeric values to perform statistical learning
df['Workclass_num'] = df['Workclass'].map({'Private':0, 'Self-emp-not-inc':1, 'Local-gov':2, 'State-gov':3, 'Self-emp-inc':4, 'Federal-gov':5, 'Without-pay':6})
df['marital_num'] = df['Marital Status'].map({'Married-civ-spouse':0, 'Never-married':1, 'Divorced':2, 'Separated':3, 'Widowed':4, 'Married-spouse-absent':5, 'Married-AF-spouse':0})
df['race_num'] = df['Race'].map({'White':0,'Black':1, 'Asian-Pac-Islander': 2, 'Amer-Indian-Eskimo':3, 'Other': 4})
df['rel_num'] = df.Relationship.map({'Not-in-family':0, 'Unmarried':0, 'Own-child':0, 'Other-relative':0, 'Husband':1, 'Wife':1})
df['over50K'] = np.where(df['Above/Below 50K'] == '<=50K', 0, 1)
df['sex_num'] = np.where(df.Sex == 'Female', 0, 1)

#Now we divide the dataset into X and Y
#['workclass_num', 'education.num', 'marital_num', 'race_num', 'sex_num', 'rel_num', 'capital.gain', 'capital.loss']
X = df[['Age','Workclass_num','marital_num','race_num','rel_num','sex_num','Education Num','Capital Gain', 'Capital Loss', 'Hours/Week']]
Y = df['over50K']

X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X,Y, test_size = 0.20, random_state = 7)
'''Data is split. Now we dont't know which ML-algorithms work better on the dataset. Hence we are going to train the model using multiple algorithms and check the accuracy to select the best model.'''
models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
'''Training over multiple ML-algorithms'''
results = []
names = []
for name, model in models:
    kfold = model_selection.KFold(n_splits = 10, random_state = 7)
    cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv = kfold, scoring = 'accuracy')
    results.append(cv_results)
    names.append(name)
    msg = "%s %f (%f)" % (name, cv_results.mean(), cv_results.std())
    print(msg)

#Looking at the results we see LR, KNN and CART give decent accuracy scores. Now we can further train over these algorithms and tune the hyperparameters to boost our accuracy by a few more points.