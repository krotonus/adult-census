# -*- coding: utf-8 -*-
#Standard Python Libraries
import io, os, sys, types, time, datetime, math, random, requests, subprocess, StringIO, tempfile

#Data Manipulation
import pandas as pd
import numpy as np

#Data Visualisation
import matplotlib.pyplot as plt
import missingno
import seaborn as sns
from pandas.tools.plotting import scatter_matrix
from mpl_toolkits.mplot3d import Axes3D

#Feature Selection and Encoding
from sklearn.feature_selection import RFE, RFECV
from sklearn.svm import SVR
from sklearn.decomposition import PCA
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, label_binarize

#Machine Learning / Model Building
import sklearn.ensemble as ske
from sklearn import datasets, model_selection, tree, preprocessing, metrics, linear_model
from sklearn.svm import LinearSVC
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LinearRegression, LogisticRegression, Ridge, Lasso, SGDClassifier
from sklearn.tree import DecisionTreeClassifier
#import tensorflow as tf

#Grid and Random Searching
import scipy.stats as st
from scipy.stats import randint as sp_randint
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV

#Metrics
from sklearn.metrics import precision_recall_fscore_support, roc_curve, auc

%matplotlib inline

#Downloading the Dataset
DATASET = ("http://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data",
    "http://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.names",
    "http://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.test")

def download_data(path = 'dataset', urls = DATASET):
    if not os.path.exists(path):
        os.mkdir(path)
        
    for url in urls:
        response = requests.get(url)
        name = os.path.basename(url)
        with open(os.path.join(path,name), 'wb') as f:
            f.write(response.content)

#download_data()

#Loading Training and Testing Datasets
headers = ['age', 'workclass', 'fnlwgt', 
           'education', 'education-num', 
           'marital-status', 'occupation', 
           'relationship', 'race', 'sex', 
           'capital-gain', 'capital-loss', 
           'hours-per-week', 'native-country', 
           'predclass']

training_raw = pd.read_csv("dataset/adult.data", header = None, names = headers, sep = ',\s', na_values = ["?"], engine = 'python')
testing_raw = pd.read_csv("dataset/adult.test", header = None, names = headers, sep = ',\s', na_values = ["?"], engine = 'python', skiprows = 1)

#Joining Datasets
dataset_raw = training_raw.append(testing_raw)
dataset_raw.reset_index(inplace = True)
dataset_raw.drop("index", inplace = True, axis = 1)

#To find the size of dataset in RAM
def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])
convert_size(dataset_raw.memory_usage().sum())

#DATA EXPLORATION

#Describing all numerical features
dataset_raw.describe()
#Describing all categorical features
dataset_raw.describe(include=['O']) #include = 'all' for all at the same time

#Plotting Distribution of each feature
def plot_distribution(dataset, cols = 5, width = 20, height = 15, hspace = 0.2, wspace = 0.5):
    plt.style.use('seaborn-whitegrid')
    fig = plt.figure(figsize = (width, height))
    fig.subplots_adjust(left = None, right= None, bottom = None, top = None, wspace = wspace, hspace = hspace)
    rows = math.ceil(float(dataset.shape[1]) / cols)
    for i, column in enumerate(dataset.columns):
        ax = fig.add_subplot(rows, cols, i+1)
        ax.set_title(column)
        if dataset.dtypes[column] == np.object:
            g = sns.countplot(y = column, data = dataset)
            substrings = [s.get_text()[:18] for s in g.get_yticklabels()]
            g.set(yticklabels = substrings)
            plt.xticks(rotation = 25)
        else:
            g = sns.distplot(dataset[column])
            plt.xticks(rotation = 25)
            
plot_distribution(dataset = dataset_raw, cols = 3, width = 20, height = 20, hspace = 0.45, wspace = 0.5)

#Number of Missing data in the dataset
missingno.matrix(dataset_raw, figsize = (30,5))
missingno.bar(dataset_raw, sort = 'ascending', figsize = (30,5))

#FEATURE CLEANING, ENGINEERING AND IMPUTATION
#Univariate Analysis
dataset_bin = pd.DataFrame() #Will contain discretised continuos variables
dataset_con = pd.DataFrame() #Will contain continuous variables 

#Changing the feature that we are going to predict to binary 1/0 value
#The boolean expression insider produces a list of True/False values which we pass to .loc[]
#This selects only those rows where the boolean expression is True. We also specify 'predclass'
#as the second argument, so we change that column's values

dataset_raw.loc[dataset_raw['predclass'] == '>50K', 'predclass'] = 1
dataset_raw.loc[dataset_raw['predclass'] == '>50K.', 'predclass'] = 1
dataset_raw.loc[dataset_raw['predclass'] == '<=50K', 'predclass'] = 0
dataset_raw.loc[dataset_raw['predclass'] == '<=50K.', 'predclass'] = 0

dataset_bin['predclass'] = dataset_raw['predclass']
dataset_con['predclass'] = dataset_raw['predclass']

#visualising the 'predclass' feature

plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize = (20,5))
sns.countplot(y='predclass', data= dataset_bin)

#Feature : Age
dataset_bin['age'] = pd.cut(dataset_raw['age'], 10) #splitting the 'age' column into bins of 10, therfore discrete
dataset_con['age'] = dataset_raw['age'] #Continuos value of the feature 'age'
#visualising the feature:age
fig = plt.figure( figsize=(20,5) )
plt.subplot(1,2,1)
sns.countplot(y = 'age', data = dataset_bin)
plt.subplot(1,2,2)
sns.distplot(dataset_con.loc[dataset_con['predclass'] == 1]['age'], kde_kws = {'label':'>50K'})
sns.distplot(dataset_con.loc[dataset_con['predclass'] == 0]['age'], kde_kws = {'label':'<=50K'})

#Feature: Workclass
plt.style.use('seaborn-whitegrid')
plt.figure(figsize = (20,3))
sns.countplot(y = 'workclass', data=dataset_raw )
#Trying to bucket some of the class so that number of classes reduces
dataset_raw.loc[dataset_raw['workclass'] == 'Without-pay', 'workclass'] = 'Not Working'
dataset_raw.loc[dataset_raw['workclass'] == 'Never-worked', 'workclass'] = 'Not Working'
dataset_raw.loc[dataset_raw['workclass'] == 'Federal-gov', 'workclass'] = 'Fed-gov'
dataset_raw.loc[dataset_raw['workclass'] == 'Local-gov', 'workclass'] = 'Non-fed-gov'
dataset_raw.loc[dataset_raw['workclass'] == 'State-gov', 'workclass'] = 'Non-fed-gov'
dataset_raw.loc[dataset_raw['workclass'] == 'Self-emp-not-inc', 'workclass'] = 'Self-emp'
dataset_raw.loc[dataset_raw['workclass'] == 'Self-emp-inc', 'workclass'] = 'Self-emp'

dataset_bin['workclass'] = dataset_raw['workclass']
dataset_con['workclass'] = dataset_raw['workclass']

#Feature: Occupation
plt.style.use('seaborn-whitegrid')
plt.figure(figsize = (20,3))
sns.countplot(y = 'occupation', data=dataset_raw )
#Grouping similar classes

dataset_raw.loc[dataset_raw['occupation'] == 'Adm-clerical', 'occupation'] = 'Admin'
dataset_raw.loc[dataset_raw['occupation'] == 'Armed-Forces', 'occupation'] = 'Millitary'
dataset_raw.loc[dataset_raw['occupation'] == 'Craft-repair', 'occupation'] = 'Manual Labour'
dataset_raw.loc[dataset_raw['occupation'] == 'Exec-managerial', 'occupation'] = 'Office Labour'
dataset_raw.loc[dataset_raw['occupation'] == 'Farming-fishing', 'occupation'] = 'Manual Labour'
dataset_raw.loc[dataset_raw['occupation'] == 'Handlers-cleaners', 'occupation'] = 'Manual Labour'
dataset_raw.loc[dataset_raw['occupation'] == 'Prof-speciality', 'occupation'] = 'Professional'
dataset_raw.loc[dataset_raw['occupation'] == 'Other-service', 'occupation'] = 'Service'
dataset_raw.loc[dataset_raw['occupation'] == 'Sales', 'occupation'] = 'Office Labour'
dataset_raw.loc[dataset_raw['occupation'] == 'Transport-moving', 'occupation'] = 'Manual Labour'
dataset_raw.loc[dataset_raw['occupation'] == 'Machine-op-inspct', 'occupation'] = 'Manual Labour'
dataset_raw.loc[dataset_raw['occupation'] == 'Tech-support', 'occupation'] = 'Office Labour'
dataset_raw.loc[dataset_raw['occupation'] == 'Protective-serv', 'occupation'] = 'Millitary'
dataset_raw.loc[dataset_raw['occupation'] == 'Priv-house-serv', 'occupation'] = 'Service'

dataset_bin['occupation'] = dataset_raw['occupation']
dataset_con['occupation'] = dataset_raw['occupation']

#Feature: Native country
plt.style.use('seaborn-whitegrid')
plt.figure(figsize = (20,5))
sns.countplot(y = 'native-country', data=dataset_raw )

#Grouping similar classes
dataset_raw.loc[dataset_raw['native-country'] == 'Cambodia', 'native-country'] = 'SE-Asia'
dataset_raw.loc[dataset_raw['native-country'] == 'Canada', 'native-country'] = 'British-Commonwealth'
dataset_raw.loc[dataset_raw['native-country'] == 'China', 'native-country'] = 'China'
dataset_raw.loc[dataset_raw['native-country'] == 'Columbia' , 'native-country'] = 'South-America'    
dataset_raw.loc[dataset_raw['native-country'] == 'Cuba', 'native-country'] = 'South-America'        
dataset_raw.loc[dataset_raw['native-country'] == 'Dominican-Republic', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Ecuador', 'native-country'] = 'South-America'     
dataset_raw.loc[dataset_raw['native-country'] == 'El-Salvador', 'native-country'] = 'South-America' 
dataset_raw.loc[dataset_raw['native-country'] == 'England', 'native-country'] = 'British-Commonwealth'
dataset_raw.loc[dataset_raw['native-country'] == 'France', 'native-country'] = 'Euro_Group_1'
dataset_raw.loc[dataset_raw['native-country'] == 'Germany', 'native-country'] = 'Euro_Group_1'
dataset_raw.loc[dataset_raw['native-country'] == 'Greece', 'native-country'] = 'Euro_Group_2'
dataset_raw.loc[dataset_raw['native-country'] == 'Guatemala', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Haiti', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Holand-Netherlands', 'native-country'] = 'Euro_Group_1'
dataset_raw.loc[dataset_raw['native-country'] == 'Honduras', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Hong', 'native-country'] = 'China'
dataset_raw.loc[dataset_raw['native-country'] == 'Hungary', 'native-country'] = 'Euro_Group_2'
dataset_raw.loc[dataset_raw['native-country'] == 'India', 'native-country'] = 'British-Commonwealth'
dataset_raw.loc[dataset_raw['native-country'] == 'Iran', 'native-country'] = 'Euro_Group_2'
dataset_raw.loc[dataset_raw['native-country'] == 'Ireland', 'native-country'] = 'British-Commonwealth'
dataset_raw.loc[dataset_raw['native-country'] == 'Italy', 'native-country'] = 'Euro_Group_1'
dataset_raw.loc[dataset_raw['native-country'] == 'Jamaica', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Japan', 'native-country'] = 'APAC'
dataset_raw.loc[dataset_raw['native-country'] == 'Laos', 'native-country'] = 'SE-Asia'
dataset_raw.loc[dataset_raw['native-country'] == 'Mexico', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Nicaragua', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Outlying-US(Guam-USVI-etc)'  , 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Peru', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Philippines', 'native-country'] = 'SE-Asia'
dataset_raw.loc[dataset_raw['native-country'] == 'Poland', 'native-country'] = 'Euro_Group_2'
dataset_raw.loc[dataset_raw['native-country'] == 'Portugal', 'native-country'] = 'Euro_Group_2'
dataset_raw.loc[dataset_raw['native-country'] == 'Puerto-Rico', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'Scotland', 'native-country'] = 'British-Commonwealth'
dataset_raw.loc[dataset_raw['native-country'] == 'South', 'native-country'] = 'Euro_Group_2'
dataset_raw.loc[dataset_raw['native-country'] == 'Taiwan', 'native-country'] = 'China'
dataset_raw.loc[dataset_raw['native-country'] == 'Thailand', 'native-country'] = 'SE-Asia'
dataset_raw.loc[dataset_raw['native-country'] == 'Trinadad&Tobago', 'native-country'] = 'South-America'
dataset_raw.loc[dataset_raw['native-country'] == 'United-States', 'native-country'] = 'United-States'
dataset_raw.loc[dataset_raw['native-country'] == 'Vietnam', 'native-country'] = 'SE-Asia'
dataset_raw.loc[dataset_raw['native-country'] == 'Yugoslavia', 'native-country'] = 'Euro_Group_2'

dataset_con['native-country'] = dataset_raw['native-country']
dataset_bin['native-country'] = dataset_raw['native-country']

#FEATURE: Education
plt.style.use('seaborn-whitegrid')
plt.figure(figsize = (20,5))
sns.countplot(y = 'education', data=dataset_raw )

dataset_raw.loc[dataset_raw['education'] == '11th','education'] = 'Dropout'
dataset_raw.loc[dataset_raw['education'] == '12th','education'] = 'Dropout'
dataset_raw.loc[dataset_raw['education'] == '7th-8th','education'] = 'Dropout'
dataset_raw.loc[dataset_raw['education'] == '1st-4th','education'] = 'Dropout'
dataset_raw.loc[dataset_raw['education'] == '10th','education'] = 'Dropout'
dataset_raw.loc[dataset_raw['education'] == '5th-6th','education'] = 'Dropout'
dataset_raw.loc[dataset_raw['education'] == '9th','education'] = 'Dropout'
dataset_raw.loc[dataset_raw['education'] == 'Preschool','education'] = 'Dropout'
dataset_raw.loc[dataset_raw['education'] == 'Assoc-voc','education'] = 'Associate'
dataset_raw.loc[dataset_raw['education'] == 'Assoc-acdm','education'] = 'Associate'
dataset_raw.loc[dataset_raw['education'] == 'Bachelors','education'] = 'Bachelors'
dataset_raw.loc[dataset_raw['education'] == 'Doctorate','education'] = 'Doctorate'
dataset_raw.loc[dataset_raw['education'] == 'Masters','education'] = 'Masters'
dataset_raw.loc[dataset_raw['education'] == 'HS-grad','education'] = 'HS-graduate'
dataset_raw.loc[dataset_raw['education'] == 'Some-college','education'] = 'HS-graduate'
dataset_raw.loc[dataset_raw['education'] == 'Prof-school','education'] = 'Professor'

dataset_bin['education'] = dataset_raw['education']
dataset_con['education'] = dataset_raw['education']


#Feature: Marital Status
plt.style.use('seaborn-whitegrid')
plt.figure(figsize = (20,5))
sns.countplot(y = 'marital-status', data=dataset_raw )

dataset_raw.loc[dataset_raw['marital-status'] == 'Never-married','marital-status'] = 'Never-married'
dataset_raw.loc[dataset_raw['marital-status'] == 'Married-AF-spouse','marital-status'] = 'Married'
dataset_raw.loc[dataset_raw['marital-status'] == 'Married-civ-spouse','marital-status'] = 'Married'
dataset_raw.loc[dataset_raw['marital-status'] == 'Married-spouse-absent','marital-status'] = 'Not-Married'
dataset_raw.loc[dataset_raw['marital-status'] == 'Separated','marital-status'] = 'Separated'
dataset_raw.loc[dataset_raw['marital-status'] == 'Divorced','marital-status'] = 'Separated'
dataset_raw.loc[dataset_raw['marital-status'] == 'Widowed','marital-status'] = 'Widowed'

dataset_con['marital-status'] = dataset_raw['marital-status']
dataset_bin['marital-status'] = dataset_raw['marital-status']

#Final Weight
dataset_bin['fnlwgt'] = pd.cut(dataset_raw['fnlwgt'],10)
dataset_con['fnlwgt'] = dataset_raw['fnlwgt']

plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize = (20,5))
sns.countplot(y = 'fnlwgt', data= dataset_bin)

#Feature : Education Number
dataset_bin['education-num'] = pd.cut(dataset_raw['education-num'], 10)
dataset_con['education-num'] = dataset_raw['education-num']

plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize = (20,5))
sns.countplot(y = 'education-num', data= dataset_bin)

#Feature: Hours per week
dataset_bin['hours-per-week'] = pd.cut(dataset_raw['hours-per-week'], 10)
dataset_con['hours-per-week'] = dataset_raw['hours-per-week']

plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize=(20,4)) 
plt.subplot(1, 2, 1)
sns.countplot(y="hours-per-week", data=dataset_bin);
plt.subplot(1, 2, 2)
sns.distplot(dataset_con['hours-per-week']);

#Feature: Capital Loss

dataset_bin['capital-loss'] = pd.cut(dataset_raw['capital-loss'], 5)
dataset_con['capital-loss'] = dataset_raw['capital-loss']

plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize=(20,4)) 
plt.subplot(1, 2, 1)
sns.countplot(y="capital-loss", data=dataset_bin);
plt.subplot(1, 2, 2)
sns.distplot(dataset_con['capital-loss']);

#Feature: Capital Gain

dataset_bin['capital-gain'] = pd.cut(dataset_raw['capital-gain'], 5)
dataset_con['capital-gain'] = dataset_raw['capital-gain']

plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize=(20,4)) 
plt.subplot(1, 2, 1)
sns.countplot(y="capital-gain", data=dataset_bin);
plt.subplot(1, 2, 2)
sns.distplot(dataset_con['capital-gain']);

#Remaining features
dataset_con['race'] = dataset_bin['race'] = dataset_raw['race']
dataset_con['sex'] = dataset_bin['sex'] = dataset_raw['sex']
dataset_con['relationship'] = dataset_bin['relationship'] = dataset_raw['relationship']

#Bivariate Analysis
#plotting the new features along with the target feature to observe the interactions

def plot_bivariate_bar(dataset, hue, cols = 5, width = 20, height=15, hspace = 0.2, wspace = 0.5):
    dataset = dataset.select_dtypes(include = [np.object])
    plt.style.use('seaborn-whitegrid')
    fig = plt.figure(figsize = (width, height))
    fig.subplots_adjust(left = None, right = None, bottom = None, top = None, wspace = wspace, hspace = hspace)
    rows = math.ceil(float(dataset.shape[1])/ cols)
    for i, column in enumerate(dataset.columns):
        ax = fig.add_subplot(rows, cols, i+1)
        ax.set_title(column)
        if dataset.dtypes[column] == np.object :
            g = sns.countplot(y = column, hue = hue, data = dataset)
            substrings = [s.get_text()[:10] for s in g.get_yticklabels()]
            g.set(yticklabels = substrings)
        else:
            g = sns.distplot(dataset[column], hue = hue)
            plt.xticks(rotation = 25)
            
plot_bivariate_bar(dataset_con, hue = 'predclass', cols = 3, width = 20, height = 12, hspace = 0.4, wspace = 0.5)

# Effect of Marital Status and Education on Income, across Marital Status.
plt.style.use('seaborn-whitegrid')
g = sns.FacetGrid(dataset_con, col = 'marital-status', size = 4, aspect = 0.7)
g = g.map(sns.boxplot, 'predclass', 'education-num')

# Historical Trends on the Sex, Education, HPW and Age impact on Income.
plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize=(20,4)) 
plt.subplot(1, 3, 1)
sns.violinplot(x='sex', y='education-num', hue='predclass', data=dataset_con, split=True, scale='count');

plt.subplot(1, 3, 2)
sns.violinplot(x='sex', y='hours-per-week', hue='predclass', data=dataset_con, split=True, scale='count');

plt.subplot(1, 3, 3)
sns.violinplot(x='sex', y='age', hue='predclass', data=dataset_con, split=True, scale='count');

#Interaction between a pair of features
sns.pairplot(dataset_con[['age', 'education-num', 'hours-per-week', 'predclass', 'capital-gain', 'capital-loss']], hue = 'predclass', diag_kind = 'kde', size = 4 )
# We try to see if any feature distinctly cluster the datapoints. Linearly separable, etc.

#Feature crossing
#Crossing Numerical features

dataset_con['age-hours'] = dataset_con['age'] * dataset_con['hours-per-week']

dataset_bin['age-hours'] = pd.cut(dataset_con['age-hours'], 10)

plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize = (20,5))
plt.subplot(1,2,1)
sns.countplot(y = 'age-hours', data = dataset_bin)
plt.subplot(1,2,2)
sns.distplot(dataset_con.loc[dataset_con['predclass'] == 1, 'age-hours'], kde_kws = {"label" : ">50K"})
sns.distplot(dataset_con.loc[dataset_con['predclass'] == 0, 'age-hours'], kde_kws = {"label" : "<=50K"})

#Crossing Categorical features
dataset_bin['sex-marital'] = dataset_con['sex-marital'] = dataset_con['sex'] + dataset_con['marital-status']

plt.style.use('seaborn-whitegrid')
fig = plt.figure(figsize = (20,5))
sns.countplot(y = 'sex-marital', data = dataset_con)

#Feature Encoding
